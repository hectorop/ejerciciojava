import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;



public class eje1 {

	public static void main(String[] args) throws IOException{
		Map<String, Integer> contactos=new TreeMap<>();
		BufferedReader linea=new BufferedReader(new InputStreamReader(System.in));
		
		boolean fin=false;
		do{
			System.out.print("> ");
			Scanner sc= new Scanner(linea.readLine());
			sc.useDelimiter(":");
			String s = sc.next();
			if(s.equals("buscar")){
				String nombre=sc.next();
				Integer telefono=contactos.get(nombre);
				if(telefono!=null){
				System.out.printf("Telefono: %d",telefono);
				}else{
					System.out.println("Contacto no encontrado");	
				}
			}
			else if(s.equals("eliminar")){
				String nombre=sc.next();
				Integer telefono=contactos.remove(nombre);
				if(telefono==null){
					System.out.println("Contacto no encontrado");
				}
				
			}
			else if(s.equals("salir")){
				fin=true;
			}
			else {
				try{
					int telefono=sc.nextInt();
					Integer anterior=contactos.put(s, telefono);
					if(anterior!=null){
						System.out.printf("El telefono %d se ha cambiado por el telefono %d", anterior, telefono);
					}
				}
				catch(InputMismatchException e){
					System.out.println("No has introducido un comando valido");
				}
				
			}
			sc.close();
		}while(!fin);

		
	}

}
